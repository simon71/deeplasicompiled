# DeepLASI

**D**eep **L**earning **A**ssisted **S**ingle-molecule **I**maging analysis: 
a software suite for automated, multi-color single-molecule data analysis.

This repository contains compiled versions of DeepLASI running without a MATLAB licence.

A detailed documentation is available at: https://deep-lasi-tutorial.readthedocs.io/en/latest/index.html

**!!!Important!!!**

MATLAB Runtime has to be installed on your computer if you want to use DeepLASI without a MATLAB license.
Run the "DeepLASI_webinstaller" to automatically install the correct Runtime version.
Alternatively, MATLAB Runtime is available for free at: https://de.mathworks.com/products/compiler/matlab-runtime.html

**!!!Important for Mac users!!!**

Macos tends to quarantine downloaded zipped Apps and you may encounter the following message when trying to run DeepLASI:

"DeepLASI Is Damaged and Can’t Be Opened. You Should Move It To The Trash."

To run DeepLASI, open the terminal app and enter the following:

xattr -c "path/to/DeepLASI.app"

For instance, if DeepLASI was downloaded to your Downloads folder, enter:

xattr -c Downloads/DeepLASI.app

or

xattr -c Downloads/DeepLASI_web_installer.app
